/* * 
  * @fecha de creación: 02/02/2020
  * @fecha de actualización: 25/05/2020
  * @autores: Francisco Javier Garrido Calleja & Diego Mompo Redoli - Domingo Savio
  * @titulo: Proyecto Desarrollo de Aplicaciones Multiplataforma
  * @descripción: Acceso mediante un sistema de autentificación con huella y sensor de tarjetas
  * @librerias: liquidCrystal.h, SoftwareSerial.h, InnovaS_Dactilar.h, SPI.h, Wire.h, RTClib.h, Keypad.h, MFRC522.h
*/

// librerías
#include <SPI.h>
#include <LiquidCrystal.h>
#include <InnovaS_Dactilar.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "RTClib.h"
#include <Keypad.h>
#include <MFRC522.h>

#define SS_PIN 53
#define RST_PIN 9
#define RELAY 8

// Instanciar RTC
RTC_DS1307 RTC;

// Declaración de variables
const byte ROWSkeys = 4; 
const byte COLSkeys = 4; 
const byte ROWSLmenu = 2; 
const byte COLSLmenu = 1; 
const byte ROWSLmenu2 = 2; 
const byte COLSLmenu2 = 1; 
const byte ROWSvolver = 1; 
const byte COLSvolver = 1; 
int readsuccess;
byte readcard[4];
byte readcard2[4];
char str[32] = "";
String comprobarStrUID[10];
char str2[32] = "";
String StrUID;
String StrUID2;
char op;
char op_tarjeta;
char op_huella;
int p = -1;
char pulsacion;
String PIN_Tecleado = "";
float numeroEstablecido; 
unsigned long interval=5000;
unsigned long previousMillis=0; 
char opcion_volver;
boolean salir = false;
int contadorUID = 0;
int contadorNoValido = 0;
const int ledPINVerde = 12;
const int ledPINRojo = 13;

char cadena[17];
int almacenaid[11];
int i=0;
int N=4;

// matrices para las teclas del teclado matricial
char keys[ROWSkeys][COLSkeys] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'.','0','#','D'}
};
char lettersmenu[ROWSLmenu][COLSLmenu] = {
  {'A'},
  {'B'}
};
char lettersmenu2[ROWSLmenu2][COLSLmenu2] = {
  {'C'},
  {'D'}
};
char volver[ROWSvolver][COLSvolver] = {
  {'#'}
};

// pines a los que van conectados los cables del teclado matricial
byte rowPins[ROWSkeys] = {40, 41, 42, 43};
byte colPins[COLSkeys] = {44, 45, 46, 47}; 
byte rowLmenu[ROWSLmenu] = {40, 41}; 
byte colLmenu[COLSLmenu] = {47}; 
byte rowLmenu2[ROWSLmenu2] = {42, 43}; 
byte colLmenu2[COLSLmenu2] = {47}; 
byte rowvolver[ROWSvolver] = {43}; 
byte colvolver[COLSvolver] = {46}; 

// Instanciar keypads
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWSkeys , COLSkeys );
Keypad keymenu = Keypad( makeKeymap(lettersmenu), rowLmenu, colLmenu, ROWSLmenu , COLSLmenu);
Keypad keymenu2 = Keypad( makeKeymap(lettersmenu2), rowLmenu2, colLmenu2, ROWSLmenu2 , COLSLmenu2);
Keypad keyvolver = Keypad( makeKeymap(volver), rowvolver, colvolver, ROWSvolver , COLSvolver);

// Clave de acceso predeterminada para validar una tarjeta
byte validKey1[4] = {0x04,' ' , ' ', 0x9A }; 

// Ponemos los pines a los que está conectado el LCD 
LiquidCrystal lcd(39, 38, 37, 36, 35, 34);
LiquidCrystal lcd2(2, 3, 4, 5, 6, 7);

//Pines del sensor de huella a Arduino
SoftwareSerial mySerial(10,11);

// Pines del lector de tarjetas
MFRC522 mfrc522(SS_PIN, RST_PIN);

// Instanciar la huella
uint32_t ClaveSensorHuella = 0;
InnovaS_Dactilar MySensorDactilar = InnovaS_Dactilar(&mySerial, ClaveSensorHuella);

/* * 
  * @fecha de creación: 15/04/2020
  * @autor: Diego Mompo Redoli - Domingo Savio
  * @nombre: isEqualArray()
  * @parametros: byte arrayA, byte arrayB, length
  * @descripción: comparar los ids de las tarjetas
  * @return: devuelve si es correcto o no el id
*/

bool isEqualArray(byte* arrayA, byte* arrayB, int length)
{
    for (int index = 0; index < length; index++){
      if(arrayA[index] == arrayB[index]){
         for(int j=3; j>= length - 4; j--){
          if(arrayA[j] == arrayB[j]){
            return true;
          }
          else{
            return false;
          }
         }
      }
      else{
        return false;
      }
    }
}
 
/* * 
  * @fecha de creación: 03/04/2020
  * @autor: Diego Mompó Redoli - Domingo Savio
  * @nombre: menu_opcion()
  * @descripción: menú para seleccionar la huella/tarjeta
  * @return: opcion
*/
char menu_opcion(){
  lcd.setCursor(0,0);
  lcd.print("Op A: Huella");
  lcd.setCursor(0,1);
  lcd.print("Op B: Tarjeta");
  char opcion = keymenu.getKey();
  
  if(opcion == 'A'){
    return opcion;
  }
  if(opcion == 'B'){
    return opcion;
  }
 }

/* * 
  * @fecha de creación: 20/04/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: menu_tarjeta()
  * @descripción: menú para registrar o validar la tarjeta
  * @return: opcion
*/
 char menu_tarjeta(){
  lcd.setCursor(0,0);
  lcd.print("Op C: Registar T.");
  lcd.setCursor(0,1);
   lcd.print("Op D: Validar T.");
        
  char opcion_tarjeta = keymenu2.getKey();
  
  if(opcion_tarjeta == 'C'){
    return opcion_tarjeta;
  }
  if(opcion_tarjeta == 'D'){
    return opcion_tarjeta;
  }
 }

/* * 
  * @fecha de creación: 22/04/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: menu_huella()
  * @descripción: menú para regustrar o validar la huella
  * @return: opcion
*/

 char menu_huella(){
  lcd.setCursor(0,0);
  lcd.print("Op C: Registar H.");
  lcd.setCursor(0,1);
  lcd.print("Op D: Validar H.");
        
  char opcion_huella = keymenu2.getKey();
  
  if(opcion_huella == 'C'){
    return opcion_huella;
  }
  if(opcion_huella == 'D'){
    return opcion_huella;
  }
 }

/* * 
  * @fecha de creación: 21/02/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: mostrar_id()
  * @descripción: mostrar ir de la tarjeta y captura la fecha
*/

 void mostrar_id(){
   int value;
   
   lcd2.setCursor(0,0);
   DateTime now = RTC.now();
   lcd2.print(now.day(), DEC);
   lcd2.print('/');
   lcd2.print(now.month(), DEC); // Mes
   lcd2.print('/');
   lcd2.print(now.year(), DEC); // Año
   lcd2.setCursor(0,1);
   lcd2.print(now.hour(), DEC);
   lcd2.print(':');
   lcd2.print(now.minute(), DEC);
   lcd2.print(':');
   lcd2.print(now.second(), DEC);
  
  Serial.println(numeroEstablecido);
  int my_id = numeroEstablecido;
 
  while (true) {
    while (! numeroEstablecido);
    // leemos el número que hemos introducido en el mySerial  
    char c = value;
   
    if (! isdigit(c)) break;
    my_id *= 10;
    my_id += c - '0';

    Serial.print("c: "); Serial.println(c);
    Serial.print("my_id: "); Serial.println(c);
  }

  Serial.print("Enrolando ID #");
  Serial.println(my_id);
}

/* * 
  * @fecha de creación: 21/02/2020
  * @autor: Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: SDACTILAR_Enrolar()
  * @parametros: id
  * @descripción: prodecimiento para registrar una huella
  * @return: false
*/
boolean SDACTILAR_Enrolar(int id) {
  
   lcd2.setCursor(0,0);
   DateTime now = RTC.now();
   lcd2.print(now.day(), DEC);
   lcd2.print('/');
   lcd2.print(now.month(), DEC); // Mes
   lcd2.print('/');
   lcd2.print(now.year(), DEC); // Año
   lcd2.setCursor(0,1);
   lcd2.print(now.hour(), DEC);
   lcd2.print(':');
   lcd2.print(now.minute(), DEC);
   lcd2.print(':');
   lcd2.print(now.second(), DEC);
    lcd.clear();

  for(int j=0; j<11; j++){
    if(almacenaid[j] == id){
    lcd.setCursor(0,0);
    lcd.print("Este id ya tiene");
    lcd.setCursor(0,1);
    lcd.print("una huella");
    return false;
    }
   }
   if(id > 162){
    lcd.clear();
    lcd.setCursor(0,0);
    
    lcd.print("El limite");
    lcd.setCursor(0,1);
    lcd.print("es de 1-162");
    delay(5000);
    return;
   }
   
  Serial.println("INGRESANDO");
  lcd.setCursor(0,0);
  lcd.print("Esperando");
  lcd.setCursor(0,1);
  lcd.print("una huella...");
  delay(8000);

// Se captura la imagen de la huella
  while (p != SDACTILAR_OK) {
    p = MySensorDactilar.CapturarImagen();
    switch (p) {
    case SDACTILAR_OK:
      Serial.println(" ");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Huella");
      lcd.setCursor(0,1);
      lcd.print("captada");
      break;
    case SDACTILAR_D_NO_DETECTADO:
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Dedo no");
      lcd.setCursor(0,1);
      lcd.print("Encontrado");
      break;
    case SDACTILAR_PAQUETE_IN_ERROR:
      Serial.println("Error al recibir el paquete");
      break;
    case SDACTILAR_IMG_ERROR:
      Serial.println("Error al determinar la imagen");
      break;
    default:
      Serial.print("Error Desconocido: 0x"); Serial.println(p, HEX);
      break;
    }
  }

// Genera la imagen de la huella captada anteriormente
  p = -1;
  p = MySensorDactilar.GenerarImg2Tz(1);
  switch (p) {
    case SDACTILAR_OK:
      Serial.println("Imagen Convertida");
      break;
    case SDACTILAR_IMGCONFUSA:
      Serial.println("Image muy confusa");
      return false;
    case SDACTILAR_PAQUETE_IN_ERROR:
      Serial.println("Paquetes Errados");
      return false;
    case SDACTILAR_RASGOSERROR:
      Serial.println("No es posible detectar los rasgos caracteriticos");
      return false;
    case SDACTILAR_IMGINVALIDA:
      Serial.println("Imagen invalida");
      return false;
    default:
      Serial.print("Error Desconocido: 0x"); Serial.println(p, HEX);
      return false;
  }

  delay(1000);
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Retire su");
  lcd.setCursor(0,1);
  lcd.print("dedo");
  delay(2000);
  p = -1;
  while (p != SDACTILAR_D_NO_DETECTADO) {
    p = MySensorDactilar.CapturarImagen();
  }
  p = -1;

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Ponga su dedo");
  lcd.setCursor(0,1);
  lcd.print("nuevamente"); 
  delay(8000);

// Verificar que la huella que se ha captado es correcta
  while (p != SDACTILAR_OK) {
    p = MySensorDactilar.CapturarImagen();
    switch (p) {
    case SDACTILAR_OK:
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Huella");
      lcd.setCursor(0,1);
      lcd.print("captada");
      break;
    case SDACTILAR_D_NO_DETECTADO:
      Serial.print(".");
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Dedo no");
      lcd.setCursor(0,1);
      lcd.print("Encontrado");
      break;
    case SDACTILAR_PAQUETE_IN_ERROR:
      Serial.println("Error al recibir el paquete");
      break;
    case SDACTILAR_IMG_ERROR:
      Serial.println("Error al determinar la imagen");
      break;
    default:
      Serial.print("Error Desconocido: 0x"); Serial.println(p, HEX);
      break;
    }
  }

// Genera la imagen de la huella captada anteriormente
  p = -1;
  p = MySensorDactilar.GenerarImg2Tz(2);
  switch (p) {
    case SDACTILAR_OK:
      Serial.println("Imagen Convertida");
      break;
    case SDACTILAR_IMGCONFUSA:
      Serial.println("Image muy confusa");
      return false;
    case SDACTILAR_PAQUETE_IN_ERROR:
      Serial.println("Paquetes Errados");
      return false;
    case SDACTILAR_RASGOSERROR:
      Serial.println("No es posible detectar los rasgos caracteriticos");
      return false;
    case SDACTILAR_IMGINVALIDA:
      Serial.println("Imagen invalida");
      return false;
    default:
      Serial.print("Error Desconocido: 0x"); Serial.println(p, HEX);
      return false;
  }

// Comprueba que las huellas que se han introducido sean las mismas
  p = -1;
  p = MySensorDactilar.CrearModelo();
  if (p == SDACTILAR_OK)
  {
    Serial.println("Muestras de Huellas Emparejadas!");
  } else if (p == SDACTILAR_PAQUETE_IN_ERROR) {
    Serial.println("Error de comunicacion");
    Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    return false;
  } else if (p == SDACTILAR_ENROLL_MISMATCH) {
    Serial.println("Muestras de Huellas NO Emparejadas!");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("Huella no");
    lcd.setCursor(0,1);
    lcd.print("registrada");
    Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    delay(5000);
    return false;
  } else {
    Serial.print("Error Desconocido: 0x"); Serial.println(p, HEX);
     Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    return false;
  }  

// Almacena la huella(id) y capta la fecha exacta en la que se ha realizado para que aparezca en el excel
  Serial.print("ID "); Serial.println(id);
  p = MySensorDactilar.GuardarModelo(id);
  if (p == SDACTILAR_OK) {
    Serial.println("EXITO - Huella Guardada!");
    lcd.clear();
    lcd.setCursor(0,0);
    lcd.print("EXITO");
    lcd.setCursor(0,1);
    lcd.print("Huella Guardada!");
    lcd2.clear();
    lcd2.setCursor(0,0);
    DateTime now = RTC.now();
    Serial.print(now.day(), DEC);// Dia
    lcd2.print(now.day(), DEC);
    Serial.print('/');
    lcd2.print('/');
    Serial.print(now.month(), DEC); // Mes
    lcd2.print(now.month(), DEC); // Mes
    Serial.print('/');
    lcd2.print('/');
    Serial.println(now.year(), DEC); // Año
    lcd2.print(now.year(), DEC); // Año
    lcd2.setCursor(0,1);
    Serial.print(now.hour(), DEC);
    lcd2.print(now.hour(), DEC);
    Serial.print(':');
    lcd2.print(':');
    Serial.print(now.minute(), DEC);
    lcd2.print(now.minute(), DEC);
    Serial.print(':');
    lcd2.print(':');
    Serial.println(now.second(), DEC);
    lcd2.print(now.second(), DEC);
     Serial.println( (String) "DATA,DATE,TIME,Registro, , " + numeroEstablecido );
     almacenaid[i] = id;
     Serial.println(almacenaid[i]);
     i++;
    delay(5000);
  } else if (p == SDACTILAR_PAQUETE_IN_ERROR) {
    Serial.println("Error de comunicacion");
     Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    return false;
  } else if (p == SDACTILAR_ERROR_UBICACION) {
    Serial.println("No se puede ubicar en la ubicacion asignada");
     Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    return false;
  } else if (p == SDACTILAR_ERROR_FLASH) {
    Serial.println("Error escribiendo en la flash");
    Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    return false;
  } else {
    Serial.print("Error Desconocido: 0x"); Serial.println(p, HEX);
    Serial.println( (String) "DATA,DATE,TIME, No Registrado, , " + numeroEstablecido );
    return false;
  }  
  return false;
}

/* * 
  * @fecha de creación: 07/04/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: getid()
  * @descripción: lee la tarjeta (id), guarda y registra la tarjeta, si no está registrada previamente. 
  * @return: true
*/

int getid(){  
  if(!mfrc522.PICC_IsNewCardPresent()){
    return 0;
  }
  if(!mfrc522.PICC_ReadCardSerial()){
    return 0;
  }
 
  Serial.println("EL UID DE LA TARJETA ESCANEADA ES:");  
 
  for(int i=0;i<4;i++){
    // almacena el codigo de la tarjeta
    readcard[i]=mfrc522.uid.uidByte[i]; 
    // convierte el id de la tarjeta. Devuelve el resultado en Bytes y lo convierte en string
    array_to_string(readcard, 4, str);
    // Guardamos en la variable SrtUID, el codigo de la tarjeta almacenado en str
    StrUID = str;
  }

// Si la tarjeta está en alguna celda del array, muestra el mensaje y no la guarda
  for(int i=0; i<9; i++){
    if(comprobarStrUID[i] == StrUID){
      lcd.setCursor(0,0);
      lcd.print("La tarjeta ya");
      lcd.setCursor(0,1);
      lcd.print("está registrada");
      delay(5000);
      return false;
    }
  }
  // Almacena en un array el código de la tarjeta
  comprobarStrUID[contadorUID] = StrUID;
  lcd.setCursor(0,0);
  lcd.print("Tarjeta");
  lcd.setCursor(0,1);
  lcd.print("Registrada");
  delay(5000);
  contadorUID++;
  
  // Se para de leer ña tarjeta
  mfrc522.PICC_HaltA();
  return 1;
}

/* * 
  * @fecha de creación: 21/04/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: getid2()
  * @descripción: lee la tarjeta (id) para validarla
  * @return: true
*/

int getid2(){  
  if(!mfrc522.PICC_IsNewCardPresent()){
    return 0;
  }
  if(!mfrc522.PICC_ReadCardSerial()){
    return 0;
  }
  Serial.println("EL UID DE LA TARJETA ESCANEADA ES:");  
  
  for(int i=0;i<4;i++){
    // almacena el codigo de la tarjeta
    readcard2[i]=mfrc522.uid.uidByte[i]; 
    // convierte el id de la tarjeta. Devuelve el resultado en Bytes y lo convierte en string
    array_to_string(readcard2, 4, str2);
    // Guardamos en la variable SrtUID, el codigo de la tarjeta almacenado en str2
    StrUID2 = str2;
  }
  // Se para de leer la tarjeta
  mfrc522.PICC_HaltA();
  return 1;
}

/* * 
  * @fecha de creación: 21/04/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: array_to_string()
  * @descripción: convierte los bytes de la tarjeta en String
  * @parametros: byte array[], length, buffer
*/

void array_to_string(byte array[], unsigned int len, char buffer[])
{
    for (unsigned int i = 0; i < len; i++)
    {
        byte nib1 = (array[i] >> 4) & 0x0F;
        byte nib2 = (array[i] >> 0) & 0x0F;
        buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
        buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
    }
    buffer[len*2] = '\0';
}
/* * 
  * @fecha de creación: 28/04/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: BucarID_Huella()
  * @descripción: se introduce la huella para buscarla y comprobar que es correcta para poder acceder.
*/
void BuscarID_Huella(){
      int p = -1;
      p = MySensorDactilar.CapturarImagen();
      if (p != SDACTILAR_OK)  return;

      p = MySensorDactilar.GenerarImg2Tz();
     if (p != SDACTILAR_OK)  return;

      p = MySensorDactilar.BusquedaRapida();
      if (p != SDACTILAR_OK)  return;

      if((MySensorDactilar.Id_Dactilar >=1) && (MySensorDactilar.Id_Dactilar <=9)){
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("Acceso");
       lcd.setCursor(0,1);
       lcd.print("Autorizado");
       Serial.println( (String) "DATA,DATE,TIME, Validado, ," + MySensorDactilar.Id_Dactilar );
       digitalWrite(RELAY, LOW);
       digitalWrite(ledPINVerde , HIGH);  
       delay(2000);
       digitalWrite(RELAY, HIGH);                  
       digitalWrite(ledPINVerde , LOW); 
      }
      else{
       lcd.clear();
       lcd.setCursor(0,0);
       lcd.print("Acceso");
       lcd.setCursor(0,1);
       lcd.print("No Autorizado");
        Serial.println( (String) "DATA,DATE,TIME, No Validado, ," + MySensorDactilar.Id_Dactilar );
       digitalWrite(ledPINRojo , HIGH);   // poner el Pin en HIGH
       delay(5000);                   // esperar un segundo
       digitalWrite(ledPINRojo , LOW);    // poner el Pin en LOW
       delay(5000);                   // esperar un segundo
      }
}


/* * 
  * @fecha de creación: 27/01/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: setup()
  * @descripción: se inicializa el serial, el lcd, el lector de tarjetas y el sensor de huella
*/

void setup()
{
    Serial.begin(9600);
    Serial.println("Sensor de Huella");
    lcd.begin(16, 2);
    lcd2.begin(16, 2);
    lcd.print("Bienvenido");
    Serial.println("Leyendo tarjeta");
    
    // Inicializa el lector de tarjetas
    SPI.begin();
    mfrc522.PCD_Init();

    //Inicializa el relé
    pinMode(RELAY, OUTPUT);
    digitalWrite(RELAY, HIGH);
    
    //Limpia todas las huellas
    memset(almacenaid,0,11*sizeof(int)); 
    
    
    // Inicializar el componente de la fecha y hora
    Wire.begin();
    RTC.begin();
    RTC.adjust(DateTime(__DATE__, __TIME__));
    
    // Setea la velocidad de comunicacion con el sensor de huella
    //Iniciar verificando los valores de 9600 y 57600
    MySensorDactilar.begin(57600);

   // Se verifica si la placa de arduino encuentra el lector de huellas. La salida la ponemos en la consola (mySerial)
   if (MySensorDactilar.VerificarClave()) {
      Serial.println("Encontrado");
      byte TotalRegistrados = (byte)MySensorDactilar.ContarRegistrados();
    Serial.print("El sensor tiene "); Serial.print(TotalRegistrados); Serial.println(" usuarios registrados");
   } else {
      Serial.println("No encontrado");
      while (1);
   }   
}

/* * 
  * @fecha de creación: 27/01/2020
  * @autor: Diego Mompó Redoli & Francisco Javier Garrido Calleja - Domingo Savio
  * @nombre: loop()
  * @descripción: función donde se realiza todo el proceso del programa
*/

void loop()                    
{
  // variable para que el loop se repita cada 5 segundos
  unsigned long currentMillis = millis();
   
  lcd2.setCursor(0,0);
  DateTime now = RTC.now();
  
  lcd2.print(now.day(), DEC);
  lcd2.print('/');
  lcd2.print(now.month(), DEC); // Mes
  lcd2.print('/');
  lcd2.print(now.year(), DEC); // Año
  lcd2.setCursor(0,1);
  lcd2.print(now.hour(), DEC);
  lcd2.print(':');
  lcd2.print(now.minute(), DEC);
  lcd2.print(':');
  lcd2.print(now.second(), DEC);

  // Menú para seleccionar la tarjeta o huella
  if ((unsigned long)(currentMillis - previousMillis) >= interval) {
  op = menu_opcion();
  Serial.println(op);
  if(op == 'B'){
  op_tarjeta = menu_tarjeta();
   Serial.println(op_tarjeta);
   opcion_volver = keyvolver.getKey();
  }
  else if(op == 'A'){
   op_huella = menu_huella();
   Serial.println(op_huella);
  }
   previousMillis = millis();
 }
  // Menú principal
  switch(op){
    case 'B':
    
    // Menú de la tarjeta
     switch(op_tarjeta){
      case 'D':
       if ((unsigned long)(currentMillis - previousMillis) >= interval) {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Validar");
          lcd.setCursor(0,1);
          lcd.print("Tarjeta");
          previousMillis = millis();
      }
      // Detectar tarjeta
        if (mfrc522.PICC_IsNewCardPresent()){
      //Seleccionamos una tarjeta
          if (mfrc522.PICC_ReadCardSerial()){
           readsuccess = getid2();
           // Valida las tarjetas que están guardadas en el array
           contadorNoValido=0;
           for(int i=0; i<9; i++){
            readsuccess = getid2();
            Serial.println(comprobarStrUID[i]);
            if(comprobarStrUID[i] == StrUID2){
              
              // Valida una tarjeta con las condiciones que se han puesto
              if(isEqualArray(mfrc522.uid.uidByte,validKey1, N)){
                  lcd.clear();
                  lcd.setCursor(0,0);
                  lcd.print("Acceso");
                  lcd.setCursor(0,1);
                  lcd.print("Autorizado");
                  // Se escribe en el Excel con los datos correspondientes
                  Serial.println( (String) "DATA,DATE,TIME, Validado, " + StrUID2 );
                  digitalWrite(RELAY, LOW);
                  digitalWrite(ledPINVerde , HIGH);  
                  delay(2000);
                  digitalWrite(RELAY, HIGH);                  
                  digitalWrite(ledPINVerde , HIGH);   
                  delay(5000);  
                  SPI.begin();
                  mfrc522.PCD_Init();
                  break;
                    
                  
               }
               // En el caso de que no tengan las condiciones correspondientes, no se valida
               else{
                 lcd.clear();
                 lcd.setCursor(0,0);
                 lcd.print("Acceso");
                 lcd.setCursor(0,1);
                 lcd.print("No Autorizado");
                 // Se escribe en el Excel con los datos correspondientes
                 Serial.println( (String) "DATA,DATE,TIME, No Validado, " + StrUID2 );
                 digitalWrite(ledPINRojo , HIGH);   
                 delay(5000);                   
                 digitalWrite(ledPINRojo , LOW);   
                 break;
                 }
             }
             // No se valida la tarjeta si no está registrada previamente
             else{
               contadorNoValido++;
             }
           }
             if( contadorNoValido == 9){
                 lcd.clear();
                 lcd.setCursor(0,0);
                 lcd.print("Acceso");
                 lcd.setCursor(0,1);
                 lcd.print("No Autorizado");
                 // Se escribe en el Excel con los datos correspondientes
                 Serial.println( (String) "DATA,DATE,TIME, No Validado, " + StrUID2 );
                 digitalWrite(ledPINRojo , HIGH);   
                 delay(5000);                   
                 digitalWrite(ledPINRojo , LOW); 
                 break;
             }
            // Se para de leer la tarjeta
            mfrc522.PICC_HaltA();
              }
            }
            // Para poder cambiar de opción, se inicializa a vacío la opción del menú
      break;

      // Se registra la tarjeta
      case 'C':

      if ((unsigned long)(currentMillis - previousMillis) >= interval) {
      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("Leyendo");
      lcd.setCursor(0,1);
      lcd.print("Tarjeta");
      previousMillis = millis();
      }
      readsuccess = getid();
      if(readsuccess){
      // Se escribe en el Excel con los datos correspondientes
      Serial.println( (String) "DATA,DATE,TIME, Registro, " + StrUID );
      }
      // Para poder cambiar de opción, se inicializa a vacío la opción del menú
      op_tarjeta = ' ';
      break;
     }
    break;
    // Menú de huella
    case 'A':
    switch(op_huella){
      // Validar la huella
      case 'D':
        if ((unsigned long)(currentMillis - previousMillis) >= interval) {
          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Validar huella");
          previousMillis = millis();
        }
        BuscarID_Huella();
     break;
     // Registrar la huella
    case 'C':
      if ((unsigned long)(currentMillis - previousMillis) >= interval) {
        lcd.clear();
        Serial.println("Ingrese el Numero (1 - 162): ");
        lcd.setCursor(0, 0);
        lcd.print("Ingrese Numero");
        lcd.setCursor(0, 1);
        lcd.print("(1-162)");
        previousMillis = millis();
      }
  numeroEstablecido = 0;  
  pulsacion = 0;
   // Recoge la tecla que se ha pulsado en el teclado matricial
  pulsacion = keypad.getKey();
  // menú para interactuar con las teclas
  switch (pulsacion) {
    

  // Almacena el número pulsado 
  case '0' ... '9':
    delay(30);
    lcd.clear();
    PIN_Tecleado += pulsacion; 
  
    lcd.setCursor(2,0);
    lcd.print("INGRESE NUMERO");
   
    lcd.setCursor(1,1);
    lcd.print(PIN_Tecleado );
    break;

   // Exporta la variable con los números que se han pulsado
  case 'B': 
    delay(30);
    lcd.clear();
    PIN_Tecleado.toCharArray( cadena,17);
    numeroEstablecido = atof (cadena);
    
    lcd.setCursor(0,0);
    lcd.print("NUMERO: ");
    lcd.setCursor(7,0);
    // Número final (id de la huella)
    lcd.print(numeroEstablecido);
 
    break;
  // Ver el número que está introducido actualmente
  case 'A' : 

    delay(30);

   lcd.clear();
   lcd.setCursor(0,0);
   lcd.print("NUMERO: ");
   lcd.setCursor(7,0);
   lcd.print(PIN_Tecleado);
  
   lcd.setCursor(0,1);
   lcd.print( "EXPORTANDO ");

   break;
 
  // Limpia la variable donde se encuentran los números introducidos
  case 'C': 
   
    delay(30);
    PIN_Tecleado = "";
    lcd.clear();
 
    lcd.setCursor(2,0);
    lcd.print("DIGITAR NUMERO");
   
    break;
    
 }
 }
  // Si hay numero final, muestra el número que se ha establecido y llama a la funcion para que registre la huella. 
  if (numeroEstablecido){
    mostrar_id();
    Serial.println(numeroEstablecido);
    while (SDACTILAR_Enrolar(numeroEstablecido));
  }
  
  break;
   }
}
