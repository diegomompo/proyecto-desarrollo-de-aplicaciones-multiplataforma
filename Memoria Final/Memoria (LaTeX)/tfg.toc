\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Contexto y justificaci\IeC {\'o}n del proyecto}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Objetivos del proyecto}{1}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Materiales y recursos empleados}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Arduino. Conceptos y funcionalidades}{4}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Evoluci\IeC {\'o}n del proyecto}{6}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Organizaci\IeC {\'o}n del trabajo en equipo}{12}{subsection.1.6}
\contentsline {subsubsection}{\numberline {1.6.1}Scrum}{12}{subsubsection.1.6.1}
\contentsline {subsubsection}{\numberline {1.6.2}GIT}{13}{subsubsection.1.6.2}
\contentsline {section}{\numberline {2}Descripci\IeC {\'o}n Funcional}{15}{section.2}
\contentsline {subsection}{\numberline {2.1}Lector de tarjetas}{16}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Tecnolog\IeC {\'\i }a RFID}{16}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Funcionamiento del RFID}{16}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Registro de la tarjeta}{17}{subsubsection.2.1.3}
\contentsline {subsubsection}{\numberline {2.1.4}Validaci\IeC {\'o}n de la tarjeta}{19}{subsubsection.2.1.4}
\contentsline {subsection}{\numberline {2.2}Sensor de huellas}{21}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Registro de la huella}{21}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Validaci\IeC {\'o}n de la huella}{23}{subsubsection.2.2.2}
\contentsline {subsection}{\numberline {2.3}Registro en el excel}{24}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Herramienta PLX-DAQ}{24}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}Procedimiento para el registro}{25}{subsubsection.2.3.2}
\contentsline {section}{\numberline {3}Valoraci\IeC {\'o}n Econ\IeC {\'o}mica}{27}{section.3}
\contentsline {section}{\numberline {4}Conclusiones y mejoras futuras}{29}{section.4}
\contentsline {subsection}{\numberline {4.1}Conclusiones}{29}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Mejoras Futuras y Propuestas}{31}{subsection.4.2}
\contentsline {section}{\numberline {5}Glosario}{32}{section.5}
\contentsline {section}{\numberline {6}Referencias Bibliogr\IeC {\'a}ficas}{33}{section.6}
\contentsline {section}{\numberline {7}Anexos}{35}{section.7}
\contentsline {subsection}{\numberline {7.1}Normas ISO Relativas a RFID}{35}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}C\IeC {\'o}digo}{36}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Opciones men\IeC {\'u} principal}{36}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Opciones del men\IeC {\'u} tarjetas}{36}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Comprobaci\IeC {\'o}n del funcionamiento del lector}{37}{subsubsection.7.2.3}
\contentsline {subsubsection}{\numberline {7.2.4}Registro de una tarjeta}{37}{subsubsection.7.2.4}
\contentsline {subsubsection}{\numberline {7.2.5}Almacenamiento del ID de la tarjeta}{38}{subsubsection.7.2.5}
\contentsline {subsubsection}{\numberline {7.2.6}Validaci\IeC {\'o}n de la tarjeta}{39}{subsubsection.7.2.6}
\contentsline {subsubsection}{\numberline {7.2.7}Extraer el c\IeC {\'o}digo de la tarjeta}{40}{subsubsection.7.2.7}
\contentsline {subsubsection}{\numberline {7.2.8}Comparaci\IeC {\'o}n bytes de la tarjeta}{40}{subsubsection.7.2.8}
\contentsline {subsubsection}{\numberline {7.2.9}Registro de la huella}{41}{subsubsection.7.2.9}
\contentsline {subsubsection}{\numberline {7.2.10}Validaci\IeC {\'o}n de la huella}{42}{subsubsection.7.2.10}
\contentsline {subsection}{\numberline {7.3}Presupuestos}{43}{subsection.7.3}
